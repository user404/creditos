var x;
x=$(document);
x.ready(inicializarEventos);

function inicializarEventos()
{
    
    nuevaObservacion(); 
    editarObservacion();
    listarObservaciones();

}

function listarObservaciones()
{
    var id = $("#terreno_id").val();
    var elemento = $('#table_observaciones thead th');
    var cont = elemento.length - 1;
    
    elemento.each( function () {
        var title = $('#table_observaciones thead th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );

        cont = cont-1;
        if (cont == 0)
        {
            return false;
        }
    } );
    
    // DataTable
    var divisor;
    var enableddisabled;
    var table = $('#table_observaciones').DataTable({
        "ajax" : Routing.generate('traer_observaciones', { id: id }),
         "order": [[ 0, "asc" ]],
         "language": {
         "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"},
          "columns": [
                { "data": "Id", "visible": false},
                { "data": "Usuario" , "visible": true},
                { "data": "Fecha" , "visible": true},
                { "data": "Tipo" , "visible": true},
                { "data": "Observacion" , "visible": true},
                {
                    "data": null,
                    "render": function ( data, type, full, meta ) {
                        var url = "{{ path('observacion_edit',{'id': 'IDPROG'}) }}";
                        var urldef = url.replace('IDPROG',data.Id);

                        var url = "{{ path('observacion_edit',{'id': 'IDPROG'}) }}";
                        var urldef = url.replace('IDPROG',data.Id);
                        if(data.Usuario == $("#usuario").val())
                        {
                         // alert('Son iguales');
                           divisor = "modalEditarObservacion";
                           enableddisabled = 'enable';
                        }
                        else{
                        //  alert('No son iguales');
                         // divisor = "";
                           divisor = "modalEditarObservacion";
                           enableddisabled = 'disabled';
                        }
                //    return "<a class='btn btn-primary' href='#'> <span class='glyphicon glyphicon-pencil'></span>"
                  return "<button id='btnB' type='button'"+ enableddisabled +" class='btn btn-primary'data-toggle='modal' data-target='#"+divisor+"'data-whatever='"+data.Id+"'><i class='glyphicon glyphicon-pencil'></i></button>"

                    ;}      
                },
                {
                    "data": null,
                    "visible": false,
                    "render": function ( data, type, full, meta ) {
                  //  return "<a class='btn btn-primary' href='" + urldef+ "''> <span class='glyphicon glyphicon-pencil'></span>"
                  return "<button id='btnB' type='button' class='btn btn-primary' data-toggle='modal' data-target='#"+divisor+"'data-whatever='"+data.Id+"'><i class='glyphicon glyphicon-edit'></i></button>"

                    ;}

        
                }
        ], 
    });
 
    table.columns().eq( 0 ).each( function ( colIdx ) {
        $( 'input', table.column( colIdx ).header() ).on( 'keyup change', function () {
            table
                .column( colIdx )
                .search( this.value )
                .draw();
        } );
    } );
}

function recargarObservacion(){
    var table = $('#table_observaciones').DataTable();
    table.ajax.url(Routing.generate('traer_observaciones', { id: $("#terreno_id").val()})).load();

    //$('#myModal').modal('hide');
}

function nuevaObservacion()
{
    $("#nuevaObservacion").on('submit',(function(e) {
                    

                e.preventDefault();                    
                $.ajax({
                url: Routing.generate('post_ajax_observacion', { id: $("#terreno_id").val()}), //"ajax_php_file.php", // Url to which the request is sendY
                type: "POST",             // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(data)   // A function to be called if request succeeds
                {
                    //  $('#id').val(data.Id);
                    //  $('#fecha').val(data.Fecha);
                    //  $('#tipo').val(data.Tipo);
                    //  $('#observacion').val(data.Observacion);
                    $("#msjNuevaObservacion").html("<div class='alert alert-success' role='alert'>"+data.Mensaje+"</div>");
                    $("#msjNuevaObservacion").fadeIn(1000);
                    $("#msjNuevaObservacion").fadeOut(2000, function(){
                         $('#modalAgregarObservacion').modal('hide');
                         $('#id').val("");
                         $('#fecha').val("");
                         $('#tipo').val("");
                         $('#observacion').val("");

                         recargarObservacion();
                    });

                     
                }
            });
            }));
}

function editarObservacion(){
$('#modalEditarObservacion').on('shown.bs.modal', function (event) {
   
//  debugger;
   var button = $(event.relatedTarget); // Button that triggered the modal
   var recipient = button.data('whatever'); // Extract info from data-* attributes
   $.ajax({
      url: Routing.generate('unaobs', { id: recipient }), //"ajax_php_file.php", // Url to which the request is sendY
      type: "GET",             // Type of request to be send, called as method
      //data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
      contentType: false,       // The content type used when sending data to the server.
      cache: false,             // To unable request pages to be cached
      processData:false,        // To send DOMDocument or non processed data file it is set to false
      success: function(data)   // A function to be called if request succeeds
      {
         var div = $('#modalEditarObservacion');
         div.find(':input[id^="observacionId"]').val(data.Id);
         div.find(':input[id^="fecha"]').val(data.Fecha);
         div.find(':input[id^="tipo"]').val(data.Tipo);
         div.find(':input[id^="observacion"]').val(data.Observacion);


        $("#observacionId").val(data.Id)
      }
      });
})


$("#editarObservacion").on('submit',(function(e) {
     var div = $('#modalEditarObservacion');
     var id_obs = div.find(':input[id^="observacionId"]').val();
                    e.preventDefault();                    
                    $.ajax({
                    url: Routing.generate('edit_ajax_observacion', { id: id_obs}), //"ajax_php_file.php", // Url to which the request is sendY
                    type: "POST",             // Type of request to be send, called as method
                    data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                    contentType: false,       // The content type used when sending data to the server.
                    cache: false,             // To unable request pages to be cached
                    processData:false,        // To send DOMDocument or non processed data file it is set to false
                    success: function(data)   // A function to be called if request succeeds
                    {

             //          var div = $('#modalEditarObservacion');
             // div.find(':input[id^="observacion_id"]').val(data.Id);
             // div.find(':input[id^="fecha"]').val(data.Fecha);
             // div.find(':input[id^="tipo"]').val(data.Tipo);
             // div.find(':input[id^="observacion"]').val(data.Observacion);

                                               // debugger;
                        //  $('#id').val(data.Id);
                        //  $('#fecha').val(data.Fecha);
                        //  $('#tipo').val(data.Tipo);
                        //  $('#observacion').val(data.Observacion);

                        $("#msjEditarObservacion").html("<div class='alert alert-success' role='alert'>"+data.Mensaje+"</div>");
                        $("#msjEditarObservacion").fadeIn(1000);
                        $("#msjEditarObservacion").fadeOut(2000, function(){
                             $('#modalEditarObservacion').modal('hide');
                             $('#id').val("");
                             $('#fecha').val("");
                             $('#tipo').val("");
                             $('#observacion').val("");

                             recargarObservacion();
                        });
    
                         
                    }
                });
 }));

}

