var x;
x=$(document);
x.ready(inicializarEventos);

function inicializarEventos()
{
    listarBeneficiarios();
    nuevoBeneficiario()
    editarBeneficiario();
    var div = $('#modalAgregarBeneficiario');
    div.find(':input[id^="documento"]').focusout(buscarTerrenos);//val(data.Id);
    bajaBeneficiario();

}

function bajaBeneficiario()
{
    $('#modalBorrarBeneficiario').on('shown.bs.modal', function (event) {
           var button = $(event.relatedTarget); 
           var recipient = button.data('whatever');
           $.ajax({
              url: Routing.generate('unbene', { id: recipient }), 
              type: "GET",
              contentType: false,
              cache: false,
              processData:false,
              success: function(data)
              {
                var div = $('#modalBorrarBeneficiario');
                div.find(':input[id^="idEliminarBeneficiario"]').val(data.Id);
                if(data.Baja == 1){
                    div.find(':input[id^="valorEliminarBeneficiario"]').val(0);
                  }else{
                    div.find(':input[id^="valorEliminarBeneficiario"]').val(1);
                  }

                

               // $("#valorEliminarBeneficiario").val(data.Baja)
              //  alert(div.find(':input[id^="valorEliminarBeneficiario"]').val());
              }
      });
});


     $("#borrarBeneficiario").on('submit',(function(e) {
         var div = $('#borrarBeneficiario');
         var id_bene = div.find(':input[id^="idEliminarBeneficiario"]').val();
         var id_bene1 = div.find(':input[id^="valorEliminarBeneficiario"]').val();
         alert(id_bene);
                        e.preventDefault();                    
                    $.ajax({
                    url: Routing.generate('edit_ajax_beneficiario', { id: id_bene}), //"ajax_php_file.php", // Url to which the request is sendY
                    type: "POST",             // Type of request to be send, called as method
                    data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                    contentType: false,       // The content type used when sending data to the server.
                    cache: false,             // To unable request pages to be cached
                    processData:false,        // To send DOMDocument or non processed data file it is set to false
                    success: function(data)   // A function to be called if request succeeds
                    {
                            $("#msjBajaBeneficiario").html("<div class='alert alert-success' role='alert'>"+data.Mensaje+"</div>");
                            $("#msjBajaBeneficiario").fadeIn(1000);
                            $("#msjBajaBeneficiario").fadeOut(2000, function(){
                                 $('#modalBorrarBeneficiario').modal('hide');
                                 $('#idEliminarBeneficiario').val("");
                                 $('#valorEliminarBeneficiario').val("");
                                 $('#motivo').val("");
                                 $('#documento').val("");
                                 recargarBeneficiario();
                            });
        
                             
                        }
                    });
     }));
}
function listarBeneficiarios()
{ 
    var elemento = $('#table_beneficiarios thead th');
    var cont = elemento.length - 2;
    var id = $("#terreno_id").val();
    elemento.each( function () {
        var title = $('#table_beneficiarios thead th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );

        cont = cont-1;
        if (cont == 0)
        {
            return false;
        }
    } );
    
    // DataTable
    var table = $('#table_beneficiarios').DataTable({
        "ajax" : Routing.generate('traer_beneficiarios', { id: id }),
         "order": [[ 0, "asc" ]],
         "language": {
         "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"},
          "columns": [
                { "data": "Id", "visible": false},
                {
                        "data": null,
                        "render": function ( data, type, full, meta ) {
                        //debugger;
                        if(data.Baja == 0 ){
                            return "<i class='glyphicon glyphicon-ok-circle' style='color:greenyellow' ></i>";
                        }else{
                            return "<i class='glyphicon glyphicon-remove-circle' style='color:red'></i>";
                        }
                        }
           
                },
                { "data": "Documento" , "visible": true},
                { "data": "Nombre" , "visible": true},
                { "data": "Mz" , "visible": true},
                { "data": "Pc" , "visible": true},
                {
                    "data": null,
                    "render": function ( data, type, full, meta ) {
                        var url = "{{ path('beneficiario_edit',{'id': 'IDPROG'}) }}";

                  //  return "<a class='btn btn-primary' href='" + urldef+ "''> <span class='glyphicon glyphicon-pencil'></span>"
                  return "<button id='btnB' type='button' class='btn btn-primary' data-toggle='modal' data-target='#modalEditarBeneficiario'data-whatever='"+data.Id+"'><i class='glyphicon glyphicon-pencil'></i></button>"

                    ;}

        
                },

                {
                    "data": null,
                    "render": function ( data, type, full, meta ) {
                        var url = "{{ path('beneficiario_edit',{'id': 'IDPROG'}) }}";

                  //  return "<a class='btn btn-primary' href='" + urldef+ "''> <span class='glyphicon glyphicon-pencil'></span>"
                  return "<button id='btnB' type='button' class='btn btn-primary' data-toggle='modal' data-target='#modalBorrarBeneficiario'data-whatever='"+data.Id+"'><i class='glyphicon glyphicon-remove'></i></button>"

                    ;}

        
                }

        ], 
    });
 
    // Apply the search
    table.columns().eq( 0 ).each( function ( colIdx ) {
        $( 'input', table.column( colIdx ).header() ).on( 'keyup change', function () {
            table
                .column( colIdx )
                .search( this.value )
                .draw();
        } );
    } );
}

function buscarTerrenos()
    {
       var div = $('#modalAgregarBeneficiario');
       var id_bene = div.find(':input[id^="documento"]').val();
       //alert(id_bene);
           $.ajax({
              url: Routing.generate('terrenosbeneficiario_ajax', { id: id_bene }), 
              type: "GET",
              contentType: false,
              cache: false,
              processData:false,
              success: function(data)
              {
                  if(data.length >= 1)
                  {
             //   alert(data[0].Mensaje);
                 $("#msjTerrenoBeneficiario").html("<div class='alert alert-danger' style='text-align:left' role='alert'><p>"+ data[0].Mensaje +"</p></div>");     
                 }    
                 else  
                 {
                  $("#msjTerrenoBeneficiario").html("<div class='alert alert-success' style='text-align:left' role='alert'>El beneficiario no esta en otros expedientes</div>");
                 }     
              }
      });

       
       
     
    }

function recargarBeneficiario(data){
    var table = $('#table_beneficiarios').DataTable();
    table.ajax.url(Routing.generate('traer_beneficiarios', { id: $("#terreno_id").val()})).load();
    //$('#myModal').modal('hide');
}

function nuevoBeneficiario(){
$("#nuevoBeneficiario").on('submit',(function(e) {
    e.preventDefault();
    $.ajax({
    url: Routing.generate('post_ajax_beneficiario', { id: $("#terreno_id").val()}), //"ajax_php_file.php", // Url to which the request is sendY
    type: "POST",             // Type of request to be send, called as method
    data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
    contentType: false,       // The content type used when sending data to the server.
    cache: false,             // To unable request pages to be cached
    processData:false,        // To send DOMDocument or non processed data file it is set to false
    success: function(data)   // A function to be called if request succeeds
    {
        
        // debugger;
        // $('#documento').val(data.Documento);
        // $('#nombre').val(data.Nombre);
        // $('#apellido').val(data.Apellido);
        // $('#email').val(data.Email);
        // $('#telefono').val(data.Telefono);
        // $('#direccion').val(data.direccion);

         $("#msjAgregarBeneficiario").html("<div class='alert alert-success' role='alert'>"+data.Mensaje+"</div>");
         $("#msjAgregarBeneficiario").fadeIn(1000);
         $("#msjAgregarBeneficiario").fadeOut(2000, function(){
             $('#modalAgregarBeneficiario').modal('hide');
             $('#id').val("");
             $('#nombre').val("");
             $('#apellido').val("");
             $('#documento').val("");
             $('#telefono').val("");
             $('#direccion').val("");
             recargarBeneficiario();
        });            
    }
});
}));
}

function editarBeneficiario(){
$('#modalEditarBeneficiario').on('shown.bs.modal', function (event) {
   var button = $(event.relatedTarget); // Button that triggered the modal
   var recipient = button.data('whatever'); // Extract info from data-* attributes
   $.ajax({
      url: Routing.generate('unbene', { id: recipient }), //"ajax_php_file.php", // Url to which the request is sendY
      type: "GET",             // Type of request to be send, called as method
      //data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
      contentType: false,       // The content type used when sending data to the server.
      cache: false,             // To unable request pages to be cached
      processData:false,        // To send DOMDocument or non processed data file it is set to false
      success: function(data)   // A function to be called if request succeeds
      {
         var div = $('#modalEditarBeneficiario');
         div.find(':input[id^="beneficiarioId"]').val(data.Id);
        div.find(':input[id^="documento"]').val(data.Documento);
        div.find(':input[id^="nombre"]').val(data.Nombre);
        div.find(':input[id^="apellido"]').val(data.Apellido);
        div.find(':input[id^="email"]').val(data.Email);
        div.find(':input[id^="telefono"]').val(data.Telefono);
        div.find(':input[id^="direccion"]').val(data.Direccion);
        div.find(':input[id^="mz"]').val(data.Mz); 
        div.find(':input[id^="pc"]').val(data.Pc);

        $("#beneficiarioId").val(data.Id)
      }
      });
})


$("#editarBeneficiario").on('submit',(function(e) {
     var div = $('#modalEditarBeneficiario');
     var id_bene = div.find(':input[id^="beneficiarioId"]').val();
                    e.preventDefault();                    
                    $.ajax({
                    url: Routing.generate('edit_ajax_beneficiario', { id: id_bene}), //"ajax_php_file.php", // Url to which the request is sendY
                    type: "POST",             // Type of request to be send, called as method
                    data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                    contentType: false,       // The content type used when sending data to the server.
                    cache: false,             // To unable request pages to be cached
                    processData:false,        // To send DOMDocument or non processed data file it is set to false
                    success: function(data)   // A function to be called if request succeeds
                    {

             //          var div = $('#modalEditarObservacion');
             // div.find(':input[id^="observacion_id"]').val(data.Id);
             // div.find(':input[id^="fecha"]').val(data.Fecha);
             // div.find(':input[id^="tipo"]').val(data.Tipo);
             // div.find(':input[id^="observacion"]').val(data.Observacion);

                                               // debugger;
                        //  $('#id').val(data.Id);
                        //  $('#fecha').val(data.Fecha);
                        //  $('#tipo').val(data.Tipo);
                        //  $('#observacion').val(data.Observacion);
                        $("#msjEditarBeneficiario").html("<div class='alert alert-success' role='alert'>"+data.Mensaje+"</div>");
                        $("#msjEditarBeneficiario").fadeIn(1000);
                        $("#msjEditarBeneficiario").fadeOut(2000, function(){
                             $('#modalEditarBeneficiario').modal('hide');
                             $('#id').val("");
                             $('#nombre').val("");
                             $('#apellido').val("");
                             $('#documento').val("");
                             $('#telefono').val("");
                             $('#direccion').val("");

                             recargarBeneficiario();
                        });
    
                         
                    }
                });
 }));
}
