<?php

namespace IPDUVCreditoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Desembolso
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Desembolso
{
    /**
     * @ORM\ManyToOne(targetEntity="Expediente_Credito", inversedBy="desembolsos")
     * @ORM\JoinColumn(name="expediente_id", referencedColumnName="id")
     */
    private $expediente;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="cuota", type="integer", nullable=true)
     */
    private $cuota;

    /**
     * @var string
     *
     * @ORM\Column(name="fecha_pagar", type="string", length=100, nullable=true)
     */
    private $fechaPagar;

    /**
     * @var float
     *
     * @ORM\Column(name="pagar", type="float", nullable=true)
     */
    private $pagar;

    /**
     * @var string
     *
     * @ORM\Column(name="fecha_pago", type="string", length=100, nullable=true)
     */
    private $fechaPago;

    /**
     * @var string
     *
     * @ORM\Column(name="pagado", type="float", nullable=true)
     */
    private $pagado;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cuota
     *
     * @param integer $cuota
     *
     * @return Desembolso
     */
    public function setCuota($cuota)
    {
        $this->cuota = $cuota;

        return $this;
    }

    /**
     * Get cuota
     *
     * @return integer
     */
    public function getCuota()
    {
        return $this->cuota;
    }

    /**
     * Set fechaPagar
     *
     * @param string $fechaPagar
     *
     * @return Desembolso
     */
    public function setFechaPagar($fechaPagar)
    {
        $this->fechaPagar = $fechaPagar;

        return $this;
    }

    /**
     * Get fechaPagar
     *
     * @return string
     */
    public function getFechaPagar()
    {
        return $this->fechaPagar;
    }

    /**
     * Set pagar
     *
     * @param float $pagar
     *
     * @return Desembolso
     */
    public function setPagar($pagar)
    {
        $this->pagar = $pagar;

        return $this;
    }

    /**
     * Get pagar
     *
     * @return float
     */
    public function getPagar()
    {
        return $this->pagar;
    }

    /**
     * Set fechaPago
     *
     * @param string $fechaPago
     *
     * @return Desembolso
     */
    public function setFechaPago($fechaPago)
    {
        $this->fechaPago = $fechaPago;

        return $this;
    }

    /**
     * Get fechaPago
     *
     * @return string
     */
    public function getFechaPago()
    {
        return $this->fechaPago;
    }

    /**
     * Set pagado
     *
     * @param float $pagado
     *
     * @return Desembolso
     */
    public function setPagado($pagado)
    {
        $this->pagado = $pagado;

        return $this;
    }

    /**
     * Get pagado
     *
     * @return float
     */
    public function getPagado()
    {
        return $this->pagado;
    }

    /**
     * Set expediente
     *
     * @param \IPDUVCreditoBundle\Entity\Expediente_Credito $expediente
     *
     * @return Desembolso
     */
    public function setExpediente(\IPDUVCreditoBundle\Entity\Expediente_Credito $expediente = null)
    {
        $this->expediente = $expediente;

        return $this;
    }

    /**
     * Get expediente
     *
     * @return \IPDUVCreditoBundle\Entity\Expediente_Credito
     */
    public function getExpediente()
    {
        return $this->expediente;
    }
}
