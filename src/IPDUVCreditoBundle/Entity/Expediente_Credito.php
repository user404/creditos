<?php

namespace IPDUVCreditoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Expediente_Credito
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Expediente_Credito
{
    /**
    *@ORM\OneToMany(targetEntity="Desembolso", mappedBy="expediente", cascade={"all"})
    **/
    private $desembolsos;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->desembolsos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add desembolso
     *
     * @param \IPDUVCreditoBundle\Entity\Desembolso $desembolso
     *
     * @return Expediente_Credito
     */
    public function addDesembolso(\IPDUVCreditoBundle\Entity\Desembolso $desembolso)
    {
        $this->desembolsos[] = $desembolso;

        return $this;
    }

    /**
     * Remove desembolso
     *
     * @param \IPDUVCreditoBundle\Entity\Desembolso $desembolso
     */
    public function removeDesembolso(\IPDUVCreditoBundle\Entity\Desembolso $desembolso)
    {
        $this->desembolsos->removeElement($desembolso);
    }

    /**
     * Get desembolsos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDesembolsos()
    {
        return $this->desembolsos;
    }
}
