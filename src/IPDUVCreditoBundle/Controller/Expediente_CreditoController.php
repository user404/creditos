<?php

namespace IPDUVCreditoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use IPDUVCreditoBundle\Entity\Expediente_Credito;
use IPDUVCreditoBundle\Form\Expediente_CreditoType;

/**
 * Expediente_Credito controller.
 *
 * @Route("/expediente_credito")
 */
class Expediente_CreditoController extends Controller
{

    /**
     * Lists all Expediente_Credito entities.
     *
     * @Route("/", name="expediente_credito")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IPDUVCreditoBundle:Expediente_Credito')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Expediente_Credito entity.
     *
     * @Route("/", name="expediente_credito_create")
     * @Method("POST")
     * @Template("IPDUVCreditoBundle:Expediente_Credito:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Expediente_Credito();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('expediente_credito_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Expediente_Credito entity.
     *
     * @param Expediente_Credito $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Expediente_Credito $entity)
    {
        $form = $this->createForm(new Expediente_CreditoType(), $entity, array(
            'action' => $this->generateUrl('expediente_credito_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Expediente_Credito entity.
     *
     * @Route("/new", name="expediente_credito_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Expediente_Credito();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Expediente_Credito entity.
     *
     * @Route("/{id}", name="expediente_credito_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVCreditoBundle:Expediente_Credito')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Expediente_Credito entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Expediente_Credito entity.
     *
     * @Route("/{id}/edit", name="expediente_credito_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVCreditoBundle:Expediente_Credito')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Expediente_Credito entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Expediente_Credito entity.
    *
    * @param Expediente_Credito $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Expediente_Credito $entity)
    {
        $form = $this->createForm(new Expediente_CreditoType(), $entity, array(
            'action' => $this->generateUrl('expediente_credito_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Expediente_Credito entity.
     *
     * @Route("/{id}", name="expediente_credito_update")
     * @Method("PUT")
     * @Template("IPDUVCreditoBundle:Expediente_Credito:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVCreditoBundle:Expediente_Credito')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Expediente_Credito entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('expediente_credito_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Expediente_Credito entity.
     *
     * @Route("/{id}", name="expediente_credito_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('IPDUVCreditoBundle:Expediente_Credito')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Expediente_Credito entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('expediente_credito'));
    }

    /**
     * Creates a form to delete a Expediente_Credito entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('expediente_credito_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
