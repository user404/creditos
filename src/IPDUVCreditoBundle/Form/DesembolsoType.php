<?php

namespace IPDUVCreditoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DesembolsoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cuota', 'integer', array( 'label'=>'Cuotas','required'=>false,'attr' => array('class'=>'form-control'))) 
            ->add('fechaPagar', 'text', array( 'label'=>'Fecha de Pago','required'=>false,'attr' => array('class'=>'form-control'))) 
            ->add('pagar', 'text', array( 'label'=>'Cantidad a pagar','required'=>false,'attr' => array('class'=>'form-control'))) 
            ->add('fechaPago', 'text', array( 'label'=>'Fecha Pagado','required'=>false,'attr' => array('class'=>'form-control'))) 
            ->add('pagado', 'text', array( 'label'=>'Monto Pagado','required'=>false,'attr' => array('class'=>'form-control'))) 
            ->add('expediente',"entity",array('label'=>'Expediente:','class'=>'IPDUVCreditoBundle:Expediente_Credito', 'property'=>'id','required'=>false,'attr' => array('class'=>'form-control') ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IPDUVCreditoBundle\Entity\Desembolso'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ipduvcreditobundle_desembolso';
    }
}
