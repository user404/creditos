<?php

namespace IPDUVCreditoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class Expediente_CreditoType extends AbstractType
{

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IPDUVCreditoBundle\Entity\Expediente_Credito'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ipduvcreditobundle_expediente_credito';
    }
}
